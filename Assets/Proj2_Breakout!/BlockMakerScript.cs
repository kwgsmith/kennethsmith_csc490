using UnityEngine;
using System.Collections;

public class BlockMakerScript : MonoBehaviour {
	
	public Transform Block;
	
	public float gridX = 20f;
	public float gridY = 20f;
	public float spacing = 2f;
	
	private float rand;
	private bool canSpawn;
	private bool readyToSpawn;
	
	GameObject[] blocks;

	// Use this for initialization
	void Start() {
		canSpawn = true;
		SpawnRandomBlocks(50);
		blocks = GameObject.FindGameObjectsWithTag("Block");
		readyToSpawn = true;
		StartCoroutine("SpawnBlocks", 30);
	} 	
	// Update is called once per frame
	void Update () {
		blocks = GameObject.FindGameObjectsWithTag("Block");
	}
	
	 IEnumerator SpawnBlocks (float seconds) {
		while(readyToSpawn){
		    yield return new WaitForSeconds(seconds);
			SpawnRandomBlocks(10);
		}
    }
	
	private void SpawnRandomBlocks(int percentChance){
		rand = Random.Range(0,100);
		for (int y = 0; y < gridY; y++) {
			for (int x = -10; x < gridX; x++) {
				Vector3 pos = new Vector3(x*2, y, 0) * spacing;
				if(rand < percentChance){
					if(blocks != null){
						foreach(GameObject b in blocks){
							if(canSpawn && b.transform.position == pos){
								canSpawn = false;
							}
						}
					}
					if (canSpawn){
						Instantiate(Block, pos, Quaternion.identity);
					}
					//reset trigger for next position in grid
					canSpawn = true;
				}
				rand = Random.Range(0, 100);
			}
		}
	}
}
