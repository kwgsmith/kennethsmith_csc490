using UnityEngine;
using System.Collections;

public class PaddleScript : MonoBehaviour {
	private float _speed = 50;
	private float _defaultSpeed = 50;
	// Use this for initialization
	void Start () {
	
	}
	public float speed
    {
        set { this._speed = value; }
        get { return this._speed; }
    }
	
	public float defaultSpeed
    {
        set { this._defaultSpeed = value; }
        get { return this._defaultSpeed; }
    }
	// Update is called once per frame
	void Update () {
		
		rigidbody.velocity = Vector3.right * Input.GetAxis("Horizontal") * _speed;
		
		Vector3 v = rigidbody.velocity;
		Vector3 topRight = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);
		Vector3 bottomLeft = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
		
		
		
		//paddle hits sides of screen
		if (bottomLeft.x > 1) {
			rigidbody.velocity = new Vector3(-Mathf.Abs(v.x),v.y, v.z);
		}
		else if (topRight.x < 0) {
			rigidbody.velocity = new Vector3(Mathf.Abs(v.x),v.y, v.z);
		}
	}
	
	
}
