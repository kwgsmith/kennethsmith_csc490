using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {
	private float defaultSpeed = 10;
	private float speed = 10;
	
	public Transform Paddle;
	private bool spawnPaddle = true;
	
	private int score = 0;
	public TextMesh scoreKeeper;
	public TextMesh endGame;
	public TextMesh lifeKeeper;
	private int lives = 3;
	public bool isLaunched = false;
	
	/*
	 * Implement launching the ball with spacebar
	 * */
	
	// Use this for initialization
	void Start () {
		endGame.renderer.enabled = false;
		rigidbody.velocity = Vector3.up * speed;		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 v = rigidbody.velocity;
		Vector3 topRight = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
		Vector3 bottomLeft = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);
		Vector3 camPos = Camera.main.transform.position;
			
		Vector3 pos = transform.position;
		if(!Input.GetButton("Jump") && !isLaunched)
		{
			Vector3 padPos = GameObject.FindGameObjectWithTag("Player").transform.position;
			rigidbody.transform.position = new Vector3(padPos.x, padPos.y + 2, pos.z);
		}
		if (Input.GetButton("Jump")){
			isLaunched = true;
		}
		if(isLaunched)
		{
			isLaunched = true;
			//ball hits top or bottom of screen
			if(topRight.y > 1){
				rigidbody.velocity = new Vector3(v.x, -Mathf.Abs(v.y), v.z);
			}
			else if(bottomLeft.y < 0){
				lives--;
				ResetSpeed();
				isLaunched = false;
				rigidbody.velocity = Vector3.up * speed;		
				if(lives <= 0)
				{
					StartCoroutine("GameOver");
				}
				lifeKeeper.text = lives.ToString();
			}
			
			//ball hits sides of screen
			else if (bottomLeft.x > 1) {
				rigidbody.velocity = new Vector3(-Mathf.Abs(v.x),v.y, v.z);
				ResetSpeed();
			}
			else if (topRight.x < 0) {
				rigidbody.velocity = new Vector3(Mathf.Abs(v.x),v.y, v.z);
				ResetSpeed();
			}
			
			//maintain speed
			if(v.magnitude < speed)
			{
				rigidbody.velocity = v.normalized * speed;
			}
		}
	}
	
	void OnCollisionEnter(Collision collision)
	{
		if(collision.gameObject.name == "Block(Clone)")
		{
			int addScore = DetermineScore(collision.gameObject);
			score += addScore;
			PowerUp(score);
			scoreKeeper.text = score.ToString();
			Destroy(collision.gameObject);
		}
		else if(collision.gameObject.name == "Paddle")
		{
			float angle = (Mathf.PI/2) + (Mathf.PI/4 * (collision.transform.position.x - transform.position.x));
			Vector3 dir = new Vector3(Mathf.Cos(angle),Mathf.Sin(angle), 0);
			rigidbody.velocity = dir;
		}
	}
	
	IEnumerator GameOver() {
		
		string message = "Game over! ";
		
		if (!PlayerPrefs.HasKey("High Score") || PlayerPrefs.GetInt("High Score") < score){
			PlayerPrefs.SetInt("High Score", score);
			PlayerPrefs.Save();
			message += "You set the high score!";
		}
		else{
			message += "You lose...Wallow in your shame.";
		}
		
		endGame.text = message;
		endGame.renderer.enabled = true;
		
		rigidbody.isKinematic = true;
		
		yield return new WaitForSeconds(5);
		
		Application.LoadLevel("breakout_plus");
	}
	private void PowerUp (int score){
		GameObject[] paddles = GameObject.FindGameObjectsWithTag("Player");
	
		speed++;
		if(paddles != null){
			foreach(GameObject p in paddles){
				p.GetComponent<PaddleScript>().speed++;
			}
		}
		if(score % 100 == 0){
			lives++;
			lifeKeeper.text = lives.ToString();
		}
		if(score >= 500){
			if(paddles != null && spawnPaddle){
				Vector3 pos = new Vector3(paddles[0].transform.position.x,19, 0);
				Instantiate(Paddle, pos, Quaternion.identity);
			}
			spawnPaddle = false;
		}
	}
	private void ResetSpeed (){
		GameObject[] paddles = GameObject.FindGameObjectsWithTag("Player");
	
		speed = defaultSpeed;
		if(paddles != null){
			foreach(GameObject p in paddles){
				p.GetComponent<PaddleScript>().speed = p.GetComponent<PaddleScript>().defaultSpeed;
			}
		}
	}
	private int DetermineScore(GameObject g){
		string color = g.GetComponent<BlockScript>().color;
		return g.GetComponent<BlockScript>().scoreByColor[color];
	}
}
