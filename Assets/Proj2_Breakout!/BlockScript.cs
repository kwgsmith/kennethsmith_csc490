using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlockScript : MonoBehaviour {
	
	private float rand;
	
	private Dictionary<string,int> _scoreByColor;
	private string _color;
	
	public Dictionary<string,int> scoreByColor
    {
        get { return _scoreByColor; }
    }
	public string color
    {
        set { this._color = value; }
        get { return this._color; }
    }
	void Awake(){
		_scoreByColor = new Dictionary<string, int>();
		_scoreByColor.Add("Red", 5);
		_scoreByColor.Add("Blue", 10);
		_scoreByColor.Add("Green", 15);
		_scoreByColor.Add("Yellow", 100);
	}
	
	// Use this for initialization
	void Start () {
		
		RandomizeColor();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void RandomizeColor(){
		rand = Random.Range(0,100);
		if(rand < 30){
			renderer.material.color = Color.green;
			color = "Green";
		}
		else if(rand < 66){
			renderer.material.color = Color.blue;
			color = "Blue";
		}
		else if(rand < 90){
			renderer.material.color = Color.red;
			color = "Red";
		}
		else{
			renderer.material.color = Color.yellow;
			color = "Yellow";
		}
		
	}
}
