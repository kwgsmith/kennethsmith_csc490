using UnityEngine;
using System.Collections;
public class ShootBullet : MonoBehaviour {
	
	public Shoot shoot;
	public Transform bullet;
	
	public float seconds;
	
	GameObject nBullet;
	Vector3 target;
	
	void OnEnable () {
    	if (shoot) {
			shoot.shoot += shootBullet;
		}
  	}
  	void OnDisable () {
    	if (shoot) {
			shoot.shoot -= shootBullet;
		}
  	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(nBullet){
			nBullet.transform.position = Vector3.Lerp(nBullet.transform.position, target, seconds);
		}
		
	}
	
	void shootBullet(){
		GameObject parent = shoot.gameObject;
		RaycastHit hit = new RaycastHit();
		
		Instantiate(bullet, new Vector3(0,0,0), Quaternion.identity);
		
		nBullet = GameObject.Find("Bullet(Clone)");
		
		Vector3 dir = parent.rigidbody.velocity.normalized;
		
		nBullet.name = "Bullet";
		nBullet.GetComponent<Sprite>().UpdateMesh();
		nBullet.transform.position = parent.transform.position + Vector3.Scale(collider.bounds.extents * 2, dir);
		
		Physics.Raycast(nBullet.transform.position, dir, out hit, shoot.distance);
		
		target = Vector3.Scale(hit.point, new Vector3(1,1,0));
		
	}
}
