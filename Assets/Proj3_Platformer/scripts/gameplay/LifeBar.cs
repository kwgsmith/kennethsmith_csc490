using UnityEngine;
using System.Collections;

public class LifeBar : MonoBehaviour {

	public Life life;
	public Transform lifeMarker;
	
	public GameObject[] lifeMarkers;
		
  	void OnEnable () {
    	if (life) {
			life.loseLife += UpdateLives;
			life.dead += OnDeath;
		}
  	}
  	void OnDisable () {
    	if (life) {
			life.loseLife -= UpdateLives;
			life.dead -= OnDeath;
		}
  	}
	
  	void Start(){
		UpdateLives();
	}
  
  	void UpdateLives () {
		
		if(this.tag != "Enemy"){
			
			lifeMarkers = GameObject.FindGameObjectsWithTag("LifeMarker");
		
			if(lifeMarkers != null){
				foreach(GameObject l in lifeMarkers)
				{
					l.tag = "";
					Destroy(l);
				}
			}
			GameObject parent = this.gameObject;
			Vector3 pos = new Vector3(1,1,1);
			
			for(int i = 0; i < life.currentLives; i++){
				Instantiate(lifeMarker, pos, Quaternion.identity);
			}
	
			lifeMarkers = GameObject.FindGameObjectsWithTag("LifeMarker");
			
			for(int i = 0; i < life.currentLives; i++){
				lifeMarkers[i].transform.parent = parent.transform;
				lifeMarkers[i].name = "Brain";
				lifeMarkers[i].GetComponent<Sprite>().UpdateMesh();
				
				lifeMarkers[i].transform.localPosition = new Vector3(i*3,0,0);
			}
		
		}
    }
	
	void OnDeath(){
		if(life.gameObject.name == "Player"){
			Application.LoadLevel("start");
			
		}
		else{
			life.gameObject.tag = "";
			Destroy(life.gameObject);
		}
	}
	
}
