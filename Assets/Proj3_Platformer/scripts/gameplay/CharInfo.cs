using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class CharInfo: ScriptableObject {

	public int lives;
	public int maxHealth;
	
}
