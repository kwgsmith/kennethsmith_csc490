using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;



[RequireComponent(typeof(Sprite))]
public class SpriteAnimation : MonoBehaviour {
	
	public int numFrames;
	public int curFrame;
	public float seconds;

	public string leftFacing;
	public string rightFacing;

	public SpriteSheet sheet;
	public bool isPlaying;
	
	public SpriteInfo[] rightAnimation;
	public SpriteInfo[] leftAnimation;
	
	public bool isLeft;
	public bool isRight;
		
	void Awake(){
		isRight = true;
	}
	
	void Start() {
		sheet.initialize();
		createAnimation();
		InvokeRepeating("play", 0f, seconds); 
	
	}
	
	public void play(){
		if(isPlaying){
			isPlaying = false;
			Sprite sprite = GetComponent<Sprite>();
			SpriteInfo info = new SpriteInfo();
			
			if(isRight){
				info = rightAnimation[curFrame];
			}
			else if(isLeft){
				info = leftAnimation[curFrame];
			}
			curFrame++;
			if(curFrame >= numFrames){
				curFrame = 0;
			}
			sprite.spriteName = info.name;
			sprite.UpdateMesh();
		}
				
    }
	
	public void createAnimation(){
		
		rightAnimation = new SpriteInfo[numFrames];
		leftAnimation = new SpriteInfo[numFrames];
		
		for(int i = 0; i < numFrames; i++){
			foreach(SpriteInfo si in sheet.sprites){
				if (si.name.Equals(leftFacing + (i+1) + ".png")){
					leftAnimation[i] = si;
				}
				if (si.name.Equals(rightFacing + (i+1) + ".png")){
					rightAnimation[i] = si;
				}
			}
		}
	}
}
