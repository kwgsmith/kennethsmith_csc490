using UnityEngine;
using System.Collections;

public class Health: MonoBehaviour {
 
 	public CharInfo character;
  	private int _currentHealth;
	
	public int currentHealth {
		get{ return _currentHealth;}
		set{ _currentHealth = value;
			 if(_currentHealth > character.maxHealth) 
				_currentHealth = character.maxHealth;
			}
	}
 	
	void Awake(){
		_currentHealth = character.maxHealth;
	}
	
  	public delegate void OnDamageTaken();
  	public event OnDamageTaken onDamageTaken = delegate {};
  	
  	void OnCollisionEnter (Collision collision) {
    	Damage damage = collision.gameObject.GetComponent<Damage>();
    	if (damage) {
			
      		currentHealth -= damage.amountOfDamage;
      		onDamageTaken();
    	}
  	}
}
