using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class SpriteInfo{
	public string name;
	public Vector2 frame;
	public Rect uvs;
	public bool rotated;
}

[Serializable]
public class SpriteSheet : ScriptableObject {
	public TextAsset sSheet;
	public Texture2D sImage;
	public Material sMaterial;
	
	public List<SpriteInfo> sprites = new List<SpriteInfo>();
	
	[SerializeField]
	private IDictionary _spriteSheet;
	
	public IDictionary spriteSheet{
		get{
			if (_spriteSheet == null) _spriteSheet = processJSON();
			if (_spriteSheet == null) Debug.LogError("sprite sheet fail on " + name);
			return _spriteSheet;
		}
		set{
			_spriteSheet = value;
		}
	}
	
	public void initialize(){
		_spriteSheet = processJSON();
		
		parseImages((IDictionary)_spriteSheet["frames"]);
	}
	
	public void parseImages(IDictionary dict){
		
		IDictionary dict2, dict3;
		
		sprites = new List<SpriteInfo>();
		
		foreach(string s in dict.Keys){
			SpriteInfo sInfo = new SpriteInfo();
		
			sInfo.name = s.ToLower();
			dict2 = dict[s] as IDictionary;
			sInfo.rotated = (bool)dict2["rotated"];
			
			dict3 = dict2["frame"] as IDictionary; 
			
			double? x = (double?)dict3["x"];
			double? y = (double?)dict3["y"];
			
			double? w = (double?)dict3["w"];
			double? h = (double?)dict3["h"];
			
			sInfo.uvs.x = (float)x/sImage.width;
			sInfo.uvs.y = (float)(1 - (y/sImage.height));
			sInfo.uvs.width = (float)w/sImage.width;
			sInfo.uvs.height = (float)h/sImage.height;
			
			
			sInfo.frame = new Vector2((float)x.Value,(float)y.Value);
			sprites.Add(sInfo);					
			
		}
				
		
		
	}
	
	public IDictionary processJSON(){
		return (IDictionary)sSheet.text.hashtableFromJson();
	}
	
	
	
}
