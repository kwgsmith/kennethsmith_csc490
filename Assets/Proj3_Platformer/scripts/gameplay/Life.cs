using UnityEngine;
using System.Collections;

public class Life: MonoBehaviour {
 
 	public CharInfo player;
	public Health health;
  	private int _currentLives;
	
	public int currentLives {
		get{ return _currentLives;}
		set{ _currentLives = value;
			 if(_currentLives > player.lives) 
				_currentLives = player.lives;
			}
	}
 	
	void OnEnable () {
    	if (health) {
			health.onDamageTaken += checkHealth;
		}
  	}
  	void OnDisable () {
    	if (health) health.onDamageTaken -= checkHealth;
  	}
	
	void Awake(){
		_currentLives = player.lives;
	}
	
  	public delegate void LoseLife();
  	public event LoseLife loseLife = delegate {};
  	
	public delegate void Dead();
  	public event Dead dead = delegate {};
  
  	void checkHealth(){
		if(health.currentHealth <= 0){
			_currentLives--;
			if(_currentLives <= 0)
			{
				dead();
			}
			else{
				health.currentHealth = player.maxHealth;
				loseLife();
			}
		}
	}
	
}
