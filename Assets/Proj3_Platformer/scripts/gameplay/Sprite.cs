using UnityEngine;
using System;
using System.Collections;


[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class Sprite : MonoBehaviour {
	
	
	public SpriteSheet sheet;
	public string spriteName;
	
	public int index;
	
	void Start(){
		index = 0;
		sheet.initialize();
		
 	}
	
	[ContextMenu("Update Mesh")]
	public void UpdateMesh(){
		GetComponent<MeshRenderer>().sharedMaterial = sheet.sMaterial;	
		
		Mesh mesh = new Mesh();
		foreach(SpriteInfo si in sheet.sprites){
			if(si.name.ToLower().Equals(spriteName)){ 
				index = sheet.sprites.IndexOf(si);
			}
		}
		
		Vector3[] verts = new Vector3[4];
		Vector2[] uvs = new Vector2[4];
		int[] tris = new int[6]{0,2,3,0,1,2};
		
		float x = sheet.sprites[index].uvs.x;
		float y = sheet.sprites[index].uvs.y;
		float w = sheet.sprites[index].uvs.width;
		float h = sheet.sprites[index].uvs.height;
		
		float vertXmod = 0f;
		float nVertYmod = 0f;
		float pVertYmod = 0f;
		float uvmod = 0f;
		
		if(name == "Player"){
			vertXmod = 4.5f;
			nVertYmod = 7f;
			pVertYmod = 4.5f;
			uvmod = 0.3f;
		}else if(name == "Enemy"){
			vertXmod = 4.5f;
			nVertYmod = 4.5f;
			pVertYmod = 4.5f;
		}else if(name == "Background"){
			vertXmod = 1f;
		    nVertYmod = 3.2f;
			pVertYmod = 3.2f;
		}
		else if(name == "Stairs"){
			vertXmod = 16f;
		    nVertYmod = 22f;
			pVertYmod = 22f;
		}else if(name == "Ledge"){
			vertXmod = 1f;
		    nVertYmod = 1f;
			pVertYmod = 1f;
		}
		else if(name == "Bullet"){
			vertXmod = 15f;
		    nVertYmod = 35f;
			pVertYmod = 35f;
		}
		else if(name == "Brain"){
			vertXmod = 3f;
		    nVertYmod = 3f;
			pVertYmod = 3f;
		}else if(name == "Ground"){
			vertXmod = 4f;
		    nVertYmod = 7f;
			pVertYmod = 7f;
		}
		
			
		
		verts[0] = new Vector3((-vertXmod*w),(h * -nVertYmod),0);
		verts[1] = new Vector3((vertXmod*w),(h * -nVertYmod),0);
		verts[2] = new Vector3((vertXmod*w),(h * pVertYmod),0);
		verts[3] = new Vector3((-vertXmod*w),(h * pVertYmod),0);
		
		uvs[1] = new Vector2(x+w,y-(h*(1 + uvmod)));
		uvs[0] = new Vector2(x,y-(h*(uvmod + 1)));
		uvs[3] = new Vector2(x,y);
		uvs[2] = new Vector2(x+w,y);
		
		mesh.vertices = verts;
		mesh.uv = uvs;
		mesh.triangles = tris;
		mesh.RecalculateNormals();
		
		GetComponent<MeshFilter>().sharedMesh = mesh;
		
	}
	
}
