using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Motor))]
[RequireComponent(typeof(Sprite))]
[RequireComponent(typeof(SpriteAnimation))]
public class EnemyController : MonoBehaviour {
	
	public float speed = 10f;
	Motor motor;
	SpriteSheet s;
	SpriteAnimation sa;
	[HideInInspector]
	public int mod, count;
	public int patrolArea;
	Shoot shooting;
	
	// Use this for initialization
	void Start () {
		motor = GetComponent<Motor>();
		sa = GetComponent<SpriteAnimation>();
		sa.isPlaying = true;
		count = 1;
		mod = 1;
		InvokeRepeating("countUp", 0f, 1f);
		patrolArea = 3;
	}
	
	// Update is called once per frame
	void Update () {
		
		rigidbody.MovePosition(new Vector3(rigidbody.position.x + (mod * speed * Time.deltaTime),rigidbody.position.y,0));
		
		if(count > patrolArea){
			mod *= -1;
			count = 1;
		}
	
		sa.isPlaying = true;
		//motor.shouldJump = Input.GetButton("Jump");
		
		if(mod < 0){
			sa.isRight = false;
			sa.isLeft = true;
		}
		else if(mod > 0){
			sa.isLeft = false;
			sa.isRight = true;
		}
		else
		{
			sa.isPlaying = false;
		}
		
	}
	private void countUp(){
			count++;
    }
	
	
	
}
