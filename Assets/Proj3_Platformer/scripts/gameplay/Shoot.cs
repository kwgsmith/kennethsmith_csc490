using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {
	
	public float distance;
	public float seconds;

	public delegate void Shooting();
  	public event Shooting shoot = delegate {};
  
	void Start(){
		
		InvokeRepeating("look",0f, seconds);
		
	}
	
	void Update(){
		
	}
	
	private void look(){
		RaycastHit hit = new RaycastHit();
		Vector3 dir = this.rigidbody.velocity.normalized;
		if(Physics.Raycast(transform.position, dir , out hit, distance))
		{
			if(hit.collider.gameObject.CompareTag("Player"))
			{
				shoot();
			}
		}
		
	}
}
