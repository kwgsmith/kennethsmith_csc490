using UnityEngine;
using System.Collections;

public class HealthBar: MonoBehaviour {
  
  	public Health health;
	public Life life;
	public Material mat;
  
  	void OnEnable () {
    	if (health)
			health.onDamageTaken += UpdateHealth;
		if (life)
			life.loseLife += UpdateHealth;
  	}
  	void OnDisable () {
    	if (health)
			health.onDamageTaken -= UpdateHealth;
		if (life)
			life.loseLife -= UpdateHealth;
  	}
	
  	void Start(){
		mat.color = Color.red;
		
	}
  
  	void UpdateHealth () {
    	transform.localScale = new Vector3(health.currentHealth*2, 1, 1);
		StartCoroutine("ColorChange", 0.3f);
		
	}
	
	IEnumerator ColorChange(int seconds){
		mat.color = Color.yellow;
		yield return new WaitForSeconds(seconds);
		mat.color = Color.red;
  	}
	
}
