using UnityEngine;
using System.Collections;

public class Anchor : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 topRight = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);
		
		transform.position = new Vector3(topRight.x - 1, topRight.y -1, 0);
	}
}
