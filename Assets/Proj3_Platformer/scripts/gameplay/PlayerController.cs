using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Motor))]
[RequireComponent(typeof(Sprite))]
[RequireComponent(typeof(SpriteAnimation))]

public class PlayerController : MonoBehaviour {
	
	public float speed = 10f;
	Motor motor;
	SpriteSheet s;
	SpriteAnimation sa;
	
	// Use this for initialization
	void Start () {
		motor = GetComponent<Motor>();
		sa = GetComponent<SpriteAnimation>();
		sa.isPlaying = false;
	}
	
	// Update is called once per frame
	void Update () {
		motor.targetVelocity.x = Input.GetAxis("Horizontal") * speed;
		if(Input.GetAxis("Horizontal") != 0.0){
			sa.isPlaying = true;
		}
		motor.shouldJump = Input.GetButton("Jump");
		
		if(motor.targetVelocity.x < 0){
			sa.isRight = false;
			sa.isLeft = true;
		}
		else if(motor.targetVelocity.x > 0){
			sa.isLeft = false;
			sa.isRight = true;
		}
		else
		{
			sa.isPlaying = false;
		}
		
		
	}
	
	
}