using UnityEngine;
using System.Collections;

public class CheckForWin : MonoBehaviour {

	public GameObject[] enemies;
	
	// Update is called once per frame
	void Update () {
		enemies = GameObject.FindGameObjectsWithTag("Enemy");
		if(enemies.Length == 0){
			Application.LoadLevel("gameover");
		}
	}
}
