using UnityEngine;
using System.Collections;

public class GameOverGUI : MonoBehaviour {

	void OnGUI () {
		
		int width = Screen.width/2;
		int height = Screen.width/2;
		int buffer = Screen.width/4;
		
		Vector2 bgPos = new Vector2((Screen.width/2) - (width/2),(Screen.height/2) - (height/2));
		
		GUI.Box(new Rect(bgPos.x,bgPos.y,width,height), "");

		if(GUI.Button(new Rect(bgPos.x + (width/4),bgPos.y + (height/5),width - buffer,height - buffer), "Return to Start Screen")) {
			Application.LoadLevel("start");
		}
		
	}
}
