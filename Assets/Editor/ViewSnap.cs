using UnityEngine;
using UnityEditor;
using System.Collections;

public class ViewSnap {
    [MenuItem("View/Front View %1")]
    static void FrontView() {
        Rotate(Vector3.forward);
    }
	
	[MenuItem("View/Back View %2")]
    static void BackView() {
        Rotate(Vector3.back);
    }
	
	[MenuItem("View/Left View %3")]
    static void LeftView() {
        Rotate(Vector3.left);
    }
	
	[MenuItem("View/Right View %4")]
    static void RightView() {
        Rotate(Vector3.right);
    }
	
	[MenuItem("View/Top View %5")]
    static void TopView() {
        Rotate(Vector3.down);
    }
	
	[MenuItem("View/Bottom View %6")]
    static void BottomView() {
        Rotate(Vector3.up);
    }
	
	[MenuItem("View/Change Projection %7")]
    static void ChangeProjection() {
        GetSceneView().orthographic = !GetSceneView().orthographic;
    }
	
	static void Rotate(Vector3 direction) {
		GetSceneView().orthographic = true;
        GetSceneView().LookAt(GetSceneView().pivot, Quaternion.LookRotation(direction));	
	}
	
    static SceneView GetSceneView()
    {
        SceneView activeSceneView = null;
        if (SceneView.lastActiveSceneView != null)
            activeSceneView = SceneView.lastActiveSceneView;
        else if (SceneView.sceneViews.Count > 0)
            activeSceneView = SceneView.sceneViews[0] as SceneView;
		return activeSceneView;
    }
}

