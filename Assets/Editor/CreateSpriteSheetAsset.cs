using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

public class CreateSpriteSheetAsset : Editor {
	[MenuItem("Assets/Create/Sprite Sheet")]
	public static void CreateAsset(){
		SpriteSheet spriteData = ScriptableObject.CreateInstance<SpriteSheet>();
		
		string path = AssetDatabase.GetAssetPath(Selection.activeObject);
		
		if (path == "")
			path = "Assets";
		else if (Path.GetExtension(path) != "")
			path = Path.GetDirectoryName(AssetDatabase.GetAssetPath(Selection.activeObject));
		
		path = AssetDatabase.GenerateUniqueAssetPath(path + "/SpriteSheet.asset");
		
		AssetDatabase.CreateAsset(spriteData, path);
		
		AssetDatabase.SaveAssets();
		EditorUtility.FocusProjectWindow();
		Selection.activeObject = spriteData;
	}
}
