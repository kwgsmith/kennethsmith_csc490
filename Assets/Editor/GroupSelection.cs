using UnityEngine;
using UnityEditor;
using System.Collections;

public class GroupSelection {
	
	[MenuItem("Utility/Group Selection %G")]
	static void GroupSelectedObjects() {
		GameObject[] selected = Selection.gameObjects;
		if (selected.Length > 0) {
			GameObject parent = new GameObject("group");
			foreach (GameObject go in selected) {
				go.transform.parent = parent.transform;	
			}
		}
	}
}
