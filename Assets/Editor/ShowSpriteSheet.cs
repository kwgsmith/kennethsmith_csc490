using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(SpriteSheet))]
public class ShowSpriteSheet : Editor {

	 public override void OnInspectorGUI() {
		DrawDefaultInspector();
		
		SpriteSheet sSheet = target as SpriteSheet;
		
		if(!sSheet.sMaterial){
			Material mat = new Material(Shader.Find ("Mobile/Particles/Alpha Blended"));
			mat.mainTexture = sSheet.sImage; 
			
			
			string matPath = AssetDatabase.GenerateUniqueAssetPath("Assets/" + sSheet.name + ".mat");
			AssetDatabase.CreateAsset(mat, matPath);
			AssetDatabase.SaveAssets();
			sSheet.sMaterial = AssetDatabase.LoadMainAssetAtPath(matPath) as Material;
		}
		
		sSheet.parseImages((IDictionary)sSheet.spriteSheet["frames"]);
		EditorGUILayout.BeginHorizontal();
		
		//EditorGUI.DrawPreviewTexture(new Rect(0, 200, sSheet.sImage.width * 3, sSheet.sImage.height * 3),sSheet.sImage);
		EditorGUILayout.EndHorizontal();
		
		
		//GUI.DrawTextureWithTexCoords(new Rect(sSheet.sprites[0].frame),sSheet.sImage, sSheet.sprites[0].uvs);
		
	}

}
