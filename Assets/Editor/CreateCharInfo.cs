using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

public class CreateCharInfoAsset : Editor {
	[MenuItem("Assets/Create/Character Information")]
	public static void CreateAsset(){
		CharInfo charData = ScriptableObject.CreateInstance<CharInfo>();
		
		string path = AssetDatabase.GetAssetPath(Selection.activeObject);
		
		if (path == "")
			path = "Assets";
		else if (Path.GetExtension(path) != "")
			path = Path.GetDirectoryName(AssetDatabase.GetAssetPath(Selection.activeObject));
		
		path = AssetDatabase.GenerateUniqueAssetPath(path + "/CharInfo.asset");
		
		AssetDatabase.CreateAsset(charData, path);
		
		AssetDatabase.SaveAssets();
		EditorUtility.FocusProjectWindow();
		Selection.activeObject = charData;
	}
}
